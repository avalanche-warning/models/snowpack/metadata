################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
import sys
import awgeo
import awio
from awsmet import SMETParser

def _inject_header(source: dict, destination: SMETParser):
    skip_fields = ["fields"] # header entries that should _not_ be overwritten
    for key, val in source.items():
        if key in skip_fields:
            continue
        if destination.has_header_entry(key):
            destination.set_header_entry(key, val)

def smet2sno(smetfile: str, outpath: str, use_first_date=False, aspects="N", angles="0", flavor="generic"):
    """Creates .sno files from .smet files.

    This function parses a .sno file plus the header of a .smet file and uses
    the smet file's meta data for the startup .sno file.

    Args:
        smetfile (str): Path to SMET file to use meta data from.
        outpath (str): Folder path to output SNO files to.
        use_first_date (bool): Use date of first data point in SMET file for the
            SNO file's profile date? Defaults to False.
        flavor (str): Semantic name for the SNO file template to use.
    """

    sno = SMETParser(os.path.join(awio.get_scriptpath(__file__), "templates", flavor + ".sno")) # sno file in SMET format
    smet = SMETParser.get_header(smetfile) # don't read any data
    _inject_header(source=smet, destination=sno)

    if use_first_date: # parse some of the file's data to get time of first measurement
        timespan = SMETParser.get_timespan(smetfile)
        sno.set_header_entry("ProfileDate", timespan[0])

    sno.save(os.path.join(outpath, sno.get_header_entry("station_id") + ".sno")) # flat field
    azi_counter = 1
    for azi in aspects.split(): # run through slope aspects (of course, they need to be at an angle)
        sno.set_header_entry("SlopeAzi", awgeo.get_aspect(azi))
        sno.set_header_entry("SlopeAngle", angles)
        sno.save(os.path.join(outpath, sno.get_header_entry("station_id") + str(azi_counter) + ".sno"))
        azi_counter += 1

if __name__ == "__main__":
    infile = sys.argv[1]
    fsz = os.stat(infile).st_size
    if fsz == 0:
        print(f'[E] Can not produce .sno file from empty file "{infile}".')
        sys.exit()
    outpath = sys.argv[2]
    _, ext = os.path.splitext(infile)

    if ext.lower() == ".smet":
        use_first_date = False
        if len(sys.argv) > 3:
            use_first_date = sys.argv[3].lower() in ["true", "1"]
        aspects = "N"
        if len(sys.argv) > 4:
            aspects = sys.argv[4]
        angles = "0"
        if len(sys.argv) > 5:
            angles = sys.argv[5]
        flavor = "generic"
        if len(sys.argv) > 6:
            flavor = sys.argv[6]
        smet2sno(infile, outpath, use_first_date, aspects, angles, flavor)
    else:
        print("[E] Can not produce .sno file from " + ext + " file.")
